﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class controller : MonoBehaviour {

    private bool targetGenerated;
    generateTargetLocation generateScript;
    GameObject Plane,Arrow;
    private Text distanceTxt;
    

    // Use this for initialization
    void Start () {
        Debug.Log("controller start");
        targetGenerated = false;
        generateScript = (generateTargetLocation)GameObject.Find("targetGenerator").GetComponent("generateTargetLocation");

        distanceTxt = (Text) GameObject.Find("distanceTxt").GetComponent<Text>();
        
        while ((!GameObject.Find("Plane Finder")) && (!GameObject.Find("distance")) && (GameObject.Find("distanceTxt")))
        {
            Debug.Log("waiting");
        }


        Arrow = GameObject.Find("navArrow");
        Plane = GameObject.Find("Plane Finder");
        Plane.SetActive(false);
        //Debug.Log("start plane setactive" + Plane.activeSelf);
        //Arrow.SetActive(false);
        //Debug.Log("start arrow setactive" + Arrow.activeSelf);
    }
	
	// Update is called once per frame
	void Update () {

        distanceTxt.text = distance.Distance.ToString();
        

        if ((GPS.flag) && (!targetGenerated)) {

            generateScript.targetLocation();
            targetGenerated = true;
        }



        Debug.Log("checking condition");
        if (Arrow)
        {
            if (distance.Distance <= 5.0f)
            {

                //Debug.Log("distance < 5");
                Arrow.SetActive(false);
                //Debug.Log("setActive arrow: " + Arrow.activeSelf);

                Plane.SetActive(true);
                //Debug.Log("setActive plane: " + Plane.activeSelf);

            }

           
            
        }


        if ((distance.Distance > 5.0f) && (!Arrow.activeSelf))
        {
            //Debug.Log("distance > 5");
            Arrow.SetActive(true);
            //Debug.Log("setActive arrow: " + Arrow.activeSelf);
            if (Plane.activeSelf)
            {

                Plane.SetActive(true);
                //Debug.Log("setActive plane: " + Plane.activeSelf);
            }

        }
    }

    public void setTargetGenerated(bool val) {

        targetGenerated = val;
    }

}