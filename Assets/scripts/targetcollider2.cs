﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class targetcollider2 : MonoBehaviour
{


    public static targetcollider2 instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void onTriggerEnter(Collider other)
    {

        moveTarget();

    }

    public void moveTarget()
    {

        Vector3 temp;
        temp.x = Random.Range(15f, 35f);
        temp.y = Random.Range(15f, 25f);
        temp.z = Random.Range(15f, 35f); 
        transform.position = new Vector3(temp.x, temp.y, temp.z);


    }



}
