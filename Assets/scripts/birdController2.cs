﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birdController2 : MonoBehaviour
{
    public Animator anim;
    public float sensitivity = 100;
    public float loudness = 0;
    AudioSource _audio;
    bool check1 = true;
    bool check2 = false;


    private Transform targetFocus2;



    // Use this for initialization
    void Start()
    {

        targetFocus2 = GameObject.FindGameObjectWithTag("target2").transform;

        anim = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
        _audio.clip = Microphone.Start(null, true, 10, 44100);
        _audio.loop = true;
        _audio.mute = true;
        while (!(Microphone.GetPosition(null) > 0)) { }
        _audio.Play();
     _audio.mute = !_audio.mute;

    }



    // Update is called once per frame
    void Update()
    {
        loudness = GetAveragedVolume() * sensitivity;
        if (loudness > 8)
        {
            //_audio.mute = !_audio.mute;
            Debug.Log("Inside Loudess");

            if (check1)
            {
                anim.Play("flyStraight");
                check1 = false;
            }

            check2 = true;
        }
        if(check1)
        {
            anim.Play("peck");
        }

    
        if (check2)
        {
            Vector3 target2 = targetFocus2.position - this.transform.position;
            Debug.Log(target2.magnitude);

            if (target2.magnitude < 1)
            {
                targetcollider2.instance.moveTarget();
            }

            transform.LookAt(targetFocus2.transform);

            float speed = Random.Range(1f, 2f);

            transform.Translate(0, 0, speed * Time.deltaTime);




        }

    }

    float GetAveragedVolume()
    {
        float[] data = new float[256];
        float a = 0;
        _audio.GetOutputData(data, 0);
        foreach (float s in data)
        {
            a = a + Mathf.Abs(s);
        }

        return a / 256;
    }
}
