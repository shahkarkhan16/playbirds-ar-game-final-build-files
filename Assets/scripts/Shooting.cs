﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour {

	public int birdcounter=0;
	public int level=1;
	private int getbird_target;
 


    public GameObject gameCamera;

    public Text text;

     int check=1;

	 int check2=1;
	 int check3=1;
	 int check4=1;
	 int check5=1;
	 int check6=1;

     
	public Text birdName;
    public static Shooting instance;
	public Transform gunFlashTarget;
	public float fireRate=1.0f;

	public bool nextShot=true;

	private string objName="";
    private int reloadCount = 0;
	public float maxDistanceRay=10000f;

AudioSource audio;
	public AudioClip[] clips;

    void Awake(){
		if(instance==null){
			instance=this;
		}
	}

    public void playSound(int sound){
		audio.clip= clips[sound];
		audio.Play();
	}

    public void Fire(){
		 Debug.Log(nextShot);
		 if(nextShot){

			StartCoroutine (takeshot());
			StartCoroutine(shootwait());
			
		}
	}
private IEnumerator shootwait(){
	yield return new WaitForSeconds (fireRate);	
     nextShot=true;
}
	 

private IEnumerator takeshot(){



		gunScript.instance.playFireanimation();
		gunScript.instance.fireSound();
		if(gunScript.instance.count == 5){
			gunScript.instance.playReloadanimation();
			
		}
         RaycastHit hit;

		gameController.instance.shotsPerRound--;


    if (Physics.Raycast(gameCamera.transform.position, gameCamera.transform.forward, out hit,maxDistanceRay)) {
			
			Vector3 birdPosition= hit.collider.gameObject.transform.position;

			if(hit.transform.name == "bluejay(Clone)")
            {
				birdcounter++;
				gameController.instance.playerScore+=5;
				gameController.instance.roundScore++;
				GameObject Blood= Instantiate (Resources.Load("CFX_Explosion_B_Smoke+Text", typeof (GameObject))) as GameObject;
		        Blood.transform.position= birdPosition;
			//	nextShot=false;

			   hit.collider.gameObject.SetActive(false);
			   StartCoroutine(Respawn(hit.collider.gameObject));
               // Destroy(hit.transform.gameObject);
                text.text = "Object Destroyed";


				
               // StartCoroutine (spawnNewBird1());
            }

            if(hit.transform.name == "copyyy(Clone)")
            {
				birdcounter++;

				gameController.instance.playerScore+=5;
				gameController.instance.roundScore++;
				GameObject Blood= Instantiate (Resources.Load("CFX_Explosion_B_Smoke+Text", typeof (GameObject))) as GameObject;
		        Blood.transform.position= birdPosition;
			//	nextShot=false;

				hit.collider.gameObject.SetActive(false);
				StartCoroutine(Respawn(hit.collider.gameObject));
               // Destroy(hit.transform.gameObject);
                text.text = "Object Destroyed";



               // StartCoroutine (spawnNewBird());
            }
			else if(hit.transform.name == "Bird_Asset(Clone)")
            {
				birdcounter++;
				gameController.instance.playerScore+=10;
				gameController.instance.roundScore++;
				GameObject Blood= Instantiate (Resources.Load("CFX_Explosion_B_Smoke+Text", typeof (GameObject))) as GameObject;
		        Blood.transform.position= birdPosition;
			//	nextShot=false;

			   hit.collider.gameObject.SetActive(false);
				StartCoroutine(Respawn(hit.collider.gameObject));
               // Destroy(hit.transform.gameObject);
                text.text = "Object Destroyed";


				
               // StartCoroutine (spawnNewBird1());
            }

			
 
	 
			
        }

	



		GameObject gunFlash= Instantiate (Resources.Load("gunFlashSmoke", typeof (GameObject))) as GameObject;
		gunFlash.transform.position= gunFlashTarget.position;
		
		
	  nextShot=false;
	  yield return new WaitForSeconds (fireRate);
	  Debug.Log(nextShot);
	
	
	}

     IEnumerator Respawn(GameObject gObj)
    {
        yield return new WaitForSeconds(2.0f);
        //gObj.transform.position = new Vector3(4.0f,2.0f,4.0f);
		gObj.transform.position =new Vector3(Random.Range(-8.0f, 11.0f), Random.Range(3.0f, 8.0f), Random.Range(-6.0f, 14.0f));
        gObj.SetActive(true);
    }

	// Use this for initialization
	void Start () {
		
     StartCoroutine (spawnNewBird());
     StartCoroutine (spawnNewBird1());
	}
	
	// Update is called once per frame
	void Update () {
	 
   	if(birdcounter==4 && check ==1 ) {
		level++;
	 	check=0;
	}
	if(birdcounter==7 && check2==1){
		level++;
		check2=0;
	}
   if(birdcounter==10 && check3==1){
		level++;	
		check3=0; 
	}

	 if(birdcounter==13 && check4==1){
		level++;	
		check4=0;
	}

	 if(birdcounter==16 && check5==1){
		level++;	
		check5=0;
	}

	 if(birdcounter==19 && check6==1){
		level++;	
		check6=0;
	}
	 

}



   

    private IEnumerator spawnNewBird(){
		yield return new WaitForSeconds(3f);

		//spawn new bird
		GameObject newBird=Instantiate (Resources.Load("copyyy", typeof(GameObject))) as GameObject;

		 
		//make the bird as a child of imagetarget

		newBird.transform.parent=GameObject.Find("Ground_Plane_Stage").transform;
		 

		//scale bird size

		newBird.transform.localScale = new Vector3(10f,10f,10f);

        //newBird.transform.position = new Vector3( Random.Range(4.0f,2.0f,4.0f);
	 	 newBird.transform.position=new Vector3(Random.Range(-3.0f, 9.0f), Random.Range(2.0f, 8.0f), Random.Range(-3.0f, 9.0f));

    }


	 private IEnumerator spawnNewBird1(){
		yield return new WaitForSeconds(3f);

		//spawn new bird
		GameObject newBird=Instantiate (Resources.Load("Bird_Asset", typeof(GameObject))) as GameObject;

		 
		//make the bird as a child of imagetarget

		newBird.transform.parent=GameObject.Find("Ground_Plane_Stage").transform;
		 

		//scale bird size

		newBird.transform.localScale = new Vector3(30f,30f,30f);

       // newBird.transform.position = new Vector3(6.0f,2.0f,6.0f);
	   newBird.transform.position=new Vector3(Random.Range(-2.0f, 9.0f), Random.Range(2.0f, 8.0f), Random.Range(-2.0f, 9.0f));
	 

    }

}
