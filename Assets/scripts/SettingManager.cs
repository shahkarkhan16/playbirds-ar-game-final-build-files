﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SettingManager : MonoBehaviour {

public Toggle fullscreenToggle;
public Dropdown resolutionDropdown;
public Dropdown textureQualityDropdown;
public Dropdown antialiasingDropdown;
public Slider musicVolumeSlider;
public Button saveButton;



public Resolution[] resolutions;
public GameSettings gameSettings;
public AudioSource musicSource;

void OnEnable(){

gameSettings = new GameSettings();

fullscreenToggle.onValueChanged.AddListener(delegate {OnFullscreenToggle();});

resolutionDropdown.onValueChanged.AddListener(delegate {OnResolutionChange();});

textureQualityDropdown.onValueChanged.AddListener(delegate {OnTextureQualityChange();});

antialiasingDropdown.onValueChanged.AddListener(delegate {OnAntialiasingChange();});

musicVolumeSlider.onValueChanged.AddListener(delegate {OnMusicChange();});

saveButton.onClick.AddListener(delegate { OnSaveButton();});


resolutions= Screen.resolutions;

foreach (Resolution resolution in resolutions)
{
	resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
}
	LoadSettings();

}

public void OnFullscreenToggle(){

gameSettings.fullscreen = Screen.fullScreen= fullscreenToggle.isOn;


}

public void OnResolutionChange(){
Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height,Screen.fullScreen);
 gameSettings.resolutionIndex=resolutionDropdown.value;
}

public void OnTextureQualityChange(){

QualitySettings.masterTextureLimit=gameSettings.textureQuality=textureQualityDropdown.value;

}

public void OnAntialiasingChange(){

QualitySettings.antiAliasing=gameSettings.antialiasing=(int)Mathf.Pow(2f, antialiasingDropdown.value);

}

public void OnMusicChange(){

musicSource.volume=gameSettings.musicVolume=musicVolumeSlider.value;
}

public void OnSaveButton(){
	SaveSettings();
}

public void SaveSettings(){
	string jsonData=JsonUtility.ToJson(gameSettings, true);
	File.WriteAllText(Application.persistentDataPath + "/gamesettings.json", jsonData);
}

public void LoadSettings(){
	
	gameSettings= JsonUtility.FromJson<GameSettings>(File.ReadAllText(Application.persistentDataPath + "/gamesettings.json"));
	musicVolumeSlider.value=gameSettings.musicVolume;
	antialiasingDropdown.value=gameSettings.antialiasing;
	textureQualityDropdown.value=gameSettings.textureQuality;
	resolutionDropdown.value=gameSettings.resolutionIndex;
	fullscreenToggle.isOn=gameSettings.fullscreen;

 
	Screen.fullScreen = gameSettings.fullscreen;

	resolutionDropdown.RefreshShownValue();
	
}



}
