﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controlscript : MonoBehaviour {

	// Use this for initialization
	AudioSource audio;
	public AudioClip[] clips;

	void Start () {
		audio=GetComponent <AudioSource> ();
		StartCoroutine (introJingle());
	}

	private void playSound(int sound){

		audio.clip= clips [sound];
		audio.Play();
	}

	private IEnumerator introJingle(){
		yield return new WaitForSeconds (3.5f);
		playSound(0);
		 
	}

	public void ChangeScene(){
		SceneManager.LoadScene("Main_Scene");
	}

	public void Quit(){
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif

	}
 
	
	 
}
