﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class targetcollider : MonoBehaviour
{


    public static targetcollider instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void onTriggerEnter(Collider other)
    {

        moveTarget();

    }

    public void moveTarget()
    {

        Vector3 temp;
        temp.x = Random.Range(15f, 55f);
        temp.y = Random.Range(15f, 25f);
        temp.z = Random.Range(15f, 55f);
        transform.position = new Vector3(temp.x, temp.y, temp.z);


    }



}
