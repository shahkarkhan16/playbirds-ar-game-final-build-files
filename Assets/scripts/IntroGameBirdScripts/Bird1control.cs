﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 


public class Bird1control : MonoBehaviour {

	private Transform targetFocus;
	// Use this for initialization
	void Start () {
		targetFocus = GameObject.FindGameObjectWithTag("targetFocus").transform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 target1 = targetFocus.position - this.transform.position;
		Debug.Log(target1.magnitude);

		if(target1.magnitude < 1){
			Target1control.instance.moveTarget();
		}

		transform.LookAt(targetFocus.transform);

		float speed = Random.Range (10f, 20f);
		transform.Translate (0, 0, speed * Time.deltaTime);
	}
}

 
