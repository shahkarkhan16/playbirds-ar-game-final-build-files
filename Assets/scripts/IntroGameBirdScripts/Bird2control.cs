﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird2control : MonoBehaviour {

	private Transform targetFocus2;
	// Use this for initialization
	void Start () {
		targetFocus2 = GameObject.FindGameObjectWithTag("targetFocus2").transform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 target2 = targetFocus2.position - this.transform.position;
		Debug.Log(target2.magnitude);

		if(target2.magnitude < 1){
			Target2control.instance.moveTarget();
		}

		transform.LookAt(targetFocus2.transform);

		float speed = Random.Range (10f, 20f);
		transform.Translate (0, 0, speed * Time.deltaTime);
	}
}

 
