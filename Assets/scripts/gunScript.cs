﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunScript : MonoBehaviour {


	AudioSource audio;
	private Animation anim;
	public static gunScript instance;
	public int count = 0;

//Gyro

	private bool gyroEnabled;
	private Gyroscope gyro;
	private GameObject deagle;
	private Quaternion rot;

	void Awake(){
		if(instance == null){
			instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		anim = gameObject.GetComponent<Animation>();

		gyroEnabled = EnableGyro();


		deagle = new GameObject("deagle");
	}

// Update is called once per frame
	public void fireSound () {
		audio.Play(); 
	}

	public void playFireanimation(){
		anim.Play("fire");
		count=count+1;
	}

	public void playReloadanimation(){
		anim.Play("reload");
		count=0;
	}

	private bool EnableGyro(){

		if(SystemInfo.supportsGyroscope){
			gyro = Input.gyro;
			gyro.enabled = true;

			deagle.transform.rotation = Quaternion.Euler(90f, 90f, 0);
			rot = new Quaternion(0, 0, 1, 0);
			return true;
		}
		return false;
	}


	private void Update(){
		if(gyroEnabled){
			transform.localRotation = gyro.attitude * rot;
		}
	}




}