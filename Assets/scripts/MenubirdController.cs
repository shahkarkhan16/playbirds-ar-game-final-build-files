﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenubirdController : MonoBehaviour {

	private Transform menufocus1;
	// Use this for initialization
	void Start () {
		menufocus1 = GameObject.FindGameObjectWithTag("menutarget1").transform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 target = menufocus1.position - this.transform.position;
		Debug.Log(target.magnitude);

		if(target.magnitude < 1){
			Menutargetcollider.instance.moveTarget();
		}

		transform.LookAt(menufocus1.transform);

		float speed = Random.Range (5f, 10f);
		transform.Translate (0, 0, speed * Time.deltaTime);
	}
}
