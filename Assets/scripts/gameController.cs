﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using UnityEngine.SceneManagement;


public class gameController : MonoBehaviour {


	public static gameController instance;

 

	//public GameObject startPanel;
	public int playerScore=0;

	public Text scoreCountText;
	public Text livesCountText;
	public Text roundTextNumber;
	public Text GameOverScoreText;

	public Text Birds_hit;

	public int round=1;

	public	int send;

	public  Text roundTextTargetText;
	public int shotsPerRound=5;
	private int lives=2;
	public GameObject[] shells;

  /*  
	public GameObject GUIScoreText;
	public GameObject GUILivesText;
	public GameObject GUICenterTarget;
	public GameObject GUIFireButton;
	public GameObject GUIGameOverpic;
	public GameObject GUIRoundText;
	public GameObject GUIGameOverPanel;
	public GameObject startPanel;
	//public GameObject GUIGun;
 */

	public GameObject GUIRoundText;
	public GameObject GUIFireButton;
	public GameObject GUIGameOverPanel;

	//RoundRules

	public int roundTargetScore =4;
 
	
	public int roundScore =0;
	private int scoreIncrement=5;
	private bool playerStarted=false;

  public int levelsss=1;






	 //AudioSource audio;
	// public AudioClip[] clips;


	void Awake(){
		if(instance==null){
			instance=this;
		}
	}


	// Use this for initialization
	void Start () {
		playerScore= int.Parse(scoreCountText.text);
		//showStartPanel();
		//audio=GetComponent<AudioSource>();
		
			 
		
	}

	/* public void playFX(int sound){
		audio.clip=clips[sound];
		audio.Play();
	}*/


	// Update is called once per frame
	void Update () {

	 
 
		if(playerStarted==false){
				StartCoroutine(playRound());
			}
			playerStarted=true;

			if(Shooting.instance.level> levelsss)
			{

				StartCoroutine(playRound());
				levelsss++;
				
			}

		 
		 showShells();

	
		if(shotsPerRound==0){
			shells [0].SetActive(false);
			StartCoroutine(loseLife());
			shotsPerRound=5;
		}

		scoreCountText.text=playerScore.ToString();
	  livesCountText.text=lives.ToString();

		Birds_hit.text=Shooting.instance.birdcounter.ToString();
    



		/* if(DefaultTrackableEventHandler.trueFalse==true){
			hideStartPanel();
			showItems(); 
			if(playerStarted==false){
				StartCoroutine(playRound());
			}
			playerStarted=true;
		}
		else{
			showStartPanel();
			hideItems();
		}
		*/


	

				 
			}
/* 
	public void showItems(){
	GUIScoreText.SetActive(true);
	GUILivesText.SetActive(true);
	GUICenterTarget.SetActive(true);
	GUIFireButton.SetActive(true);
	GUIGameOverpic.SetActive(true);
	GUIGameOverPanel.SetActive(true);
	//GUIGun.SetActive(true);
	 
	
	}


public void hideItems(){
	GUIScoreText.SetActive(false);
	GUILivesText.SetActive(false);
	GUICenterTarget.SetActive(false);
	GUIFireButton.SetActive(false);
	GUIGameOverpic.SetActive(false);
	GUIGameOverPanel.SetActive(false);
	 //GUIGun.SetActive(false);
	
	}
*/

	public IEnumerator playRound(){
		yield return new WaitForSeconds(2f);
		if(lives!=0){
	  roundTextNumber.text=Shooting.instance.level.ToString();
		roundTextTargetText.text="Shoot "+roundTargetScore+ " Birds ";
		roundTargetScore= roundTargetScore+3;
		GUIRoundText.SetActive(true);
		shotsPerRound=5;
		lives=2;
		//playFX(0);
		StartCoroutine(hideRoundText());
		}
	}

 
	 
	
 

	private IEnumerator hideRoundText (){
		yield return new WaitForSeconds (4);
		GUIRoundText.SetActive(false);
	}

	public void showShells(){
		if(shotsPerRound==5){
			shells[0].SetActive(true);
			shells[1].SetActive(true);
			shells[2].SetActive(true);
			shells[3].SetActive(true);
			shells[4].SetActive(true);
			
		}
		else if(shotsPerRound==4){
				shells[0].SetActive(true);
			shells[1].SetActive(true);
			shells[2].SetActive(true);
			shells[3].SetActive(true);
			shells[4].SetActive(false);
		}
		else if(shotsPerRound==3){
			shells[0].SetActive(true);
			shells[1].SetActive(true);
			shells[2].SetActive(true);
			shells[3].SetActive(false);
			shells[4].SetActive(false);
		}
			else if(shotsPerRound==2){
			shells[0].SetActive(true);
			shells[1].SetActive(true);
			shells[2].SetActive(false);
			shells[3].SetActive(false);
			shells[4].SetActive(false);
		}

	    else if(shotsPerRound==1){
			shells[0].SetActive(true);
			shells[1].SetActive(false);
			shells[2].SetActive(false);
			shells[3].SetActive(false);
			shells[4].SetActive(false);
		}



	}

	/* 	private void showStartPanel(){
		startPanel.SetActive(true);

	}
	
	
	private void hideStartPanel(){
		startPanel.SetActive(false);
		
	}
*/

private IEnumerator loseLife(){
	
	lives--;
	if(lives==0){
		GUIFireButton.SetActive(false);
		GUIGameOverPanel.SetActive(true);
		GameOverScoreText.text=playerScore.ToString();
		lives=0;
	
	}
	yield return new WaitForSeconds(0);
}

}
