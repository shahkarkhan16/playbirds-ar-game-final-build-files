﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birdController : MonoBehaviour
{

    private Transform targetFocus;
     float speed;
     
    // Use this for initialization
    void Start()
    {
        targetFocus = GameObject.FindGameObjectWithTag("target").transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = targetFocus.position - this.transform.position;
        Debug.Log(target.magnitude);

        if (target.magnitude < 1)
        {
            targetcollider.instance.moveTarget();
        }

        transform.LookAt(targetFocus.transform);

          if(Shooting.instance.level <=2){
         speed = Random.Range(2f,3f);
        transform.Translate(0, 0, speed * Time.deltaTime);
        }           
        else{
              
            speed = Random.Range(2f,3f);
            transform.Translate(0, 0, speed * Time.deltaTime* 2f);
            
             
        }
    }
}
